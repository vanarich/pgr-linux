# step 1

download pgr-framework for linux from [here](https://cent.felk.cvut.cz/courses/PGR/framework/pgr-framework-linux-macos.zip)

extract the framework-master directory where you want to keep it

run "cmake ." and "make" to get the **libpgr.so** library

# step 2

install **freeglut** with your package manager

might need to install **assimp**, **devil** and maybe even something else

# step 3 (autism.jpg)

go to framework-master/assimp/lib

copy **libassimp.so.3.0.1264** and rename it to **libassimp.so.3**

# step 4

paste the **CMakeLists.txt** provided by this repo into your project folder (not framework-master)
Change the **PGR_ROOT** variable to point to the framework-master directory.

Don't forget to aswell change the project name and the **SOURCES** vaariable to suit you.

Run "cmake ." and "make"

Hope it works **o_o**
